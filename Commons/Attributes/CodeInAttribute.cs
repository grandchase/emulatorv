﻿using System;

namespace Commons.Attributes
{
    public class CodeInAttribute : Attribute
    {
        public CodeInAttribute(ushort code)
        {
            Code = code;
        }

        public CodeInAttribute(object code)
        {
            Code = (ushort) code;
        }

        public ushort Code { get; }
    }
}