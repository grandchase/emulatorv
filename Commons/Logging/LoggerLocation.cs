﻿namespace Commons.Logging
{
    public enum LoggerLocation
    {
        Center,
        Lua,
        Commons,
        Networking,
        Game
    }
}