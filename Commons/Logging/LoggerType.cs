﻿namespace Commons.Logging
{
    public enum LoggerType
    {
        Info,
        Lua,
        Debug,
        Warn,
        Error,
        Write
    }
}