﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Timers;

namespace Commons.Logging
{
    public static class LogManager
    {
        private static readonly ConcurrentDictionary<string, Logger> Instances = new ConcurrentDictionary<string, Logger>();
        private static readonly Queue<LoggerMessage> Queue = new Queue<LoggerMessage>();
        private static readonly Timer Timer;

        static LogManager()
        {
            Timer = new Timer
            {
                Interval = 100,
                Enabled = true
            };
            Timer.Elapsed += (sender, e) => Dequeue();
            Timer.Start();
        }
        
        public static Logger Instance(LoggerLocation location, string name)
        {
            return Instances.GetOrAdd(name, x => new Logger(location, name));
        }

        internal static void Enqueue(LoggerMessage loggerMessage)
        {
            lock (Queue)
            {
                Queue.Enqueue(loggerMessage);
            }
        }

        private static void Dequeue()
        {
            
            lock (Queue)
            {
                if (Queue.Count <= 0) return;
                var message = Queue.Dequeue();
                lock (Queue)
                {
                    switch (message.Type)
                    {
                        case LoggerType.Info:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write($" {message.Type.ToString().ToUpper()}  ");
                            break;
                        case LoggerType.Lua:
                            Console.ForegroundColor = ConsoleColor.DarkCyan;
                            Console.Write($" {message.Type.ToString().ToUpper()}   ");
                            break;
                        case LoggerType.Debug:
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.Write($" {message.Type.ToString().ToUpper()} ");
                            break;
                        case LoggerType.Warn:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write($" {message.Type.ToString().ToUpper()} ");
                            break;
                        case LoggerType.Error:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write($" {message.Type.ToString().ToUpper()} ");
                            break;
                    }

                    switch (message.Location)
                    {
                        case LoggerLocation.Center:
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.Write($"{LoggerLocation.Center.ToString()}      ");
                            break;
                        case LoggerLocation.Commons:
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.Write($"{LoggerLocation.Commons.ToString()}  ");
                            break;
                        case LoggerLocation.Lua:
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write($"{LoggerLocation.Lua.ToString()}         ");
                            break;
                        case LoggerLocation.Networking:
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.Write($"{LoggerLocation.Networking.ToString()}  ");
                            break;
                    }

                    if (message.Location == LoggerLocation.Lua)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                    }

                    Console.Write($"[{message.Name}] ");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine(message.Message);
                }
            }
        }

        public static void Dispose()
        {
            Timer?.Dispose();
        }
    }
}