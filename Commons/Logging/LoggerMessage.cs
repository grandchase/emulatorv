﻿using System;

namespace Commons.Logging
{
    public class LoggerMessage
    {
        public LoggerLocation Location { get; }
        public string Name { get; }
        public string Message { get; }
        public LoggerType Type { get; }
        public DateTime DateTime { get; }
        public LoggerMessage(LoggerLocation location, string name, string message, LoggerType loggerType)
        {
            Location = location;
            Name = name;
            Message = message;
            Type = loggerType;
            DateTime = DateTime.Now;
        }
    }
}