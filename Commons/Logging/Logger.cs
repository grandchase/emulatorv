﻿namespace Commons.Logging
{
    public class Logger
    {
        private LoggerLocation Location { get; }
        private string Name { get; }

        public Logger(LoggerLocation location, string name)
        {
            Location = location;
            Name = name;
        }

        public void Info(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Info));
        }

        public void Lua(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Lua));
        }

        public void Debug(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Debug));
        }

        public void Warn(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Warn));
        }

        public void Error(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Error));
        }

        public void Write(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Write));
        }
    }
}