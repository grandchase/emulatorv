﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commons.Utilities
{
    public static class ListToString
    {
        public static string Get(IEnumerable<List<string>> list)
        {
            var stringBuilder = new StringBuilder("[");
            var enumerable = list as List<string>[] ?? list.ToArray();
            foreach (var intern in enumerable)
            {
                stringBuilder.Append("[");
                var itemBuilder = new StringBuilder();
                foreach (var item in intern)
                {
                    if (itemBuilder.Length > 0) itemBuilder.Append(", ");
                    itemBuilder.Append(item);
                }
                stringBuilder.Append(itemBuilder);
                stringBuilder.Append("]");
                if (!enumerable.Last().Equals(intern)) stringBuilder.Append(", ");
            }
            return stringBuilder.Append("]").ToString();
        }
        public static string Get(Dictionary<string, int> list)
        {
            var stringBuilder = new StringBuilder("[");
            foreach (var intern in list)
            {
                stringBuilder.Append("[");
                stringBuilder.Append($"{intern.Key} : {intern.Value}");
                stringBuilder.Append("]");
                if (!list.Last().Equals(intern)) stringBuilder.Append(", ");
            }
            return stringBuilder.Append("]").ToString();
        }
        public static string Get(Dictionary<int, string> list)
        {
            var stringBuilder = new StringBuilder("[");
            foreach (var intern in list)
            {
                stringBuilder.Append("[");
                stringBuilder.Append($"{intern.Key} : {intern.Value}");
                stringBuilder.Append("]");
                if (!list.Last().Equals(intern)) stringBuilder.Append(", ");
            }
            return stringBuilder.Append("]").ToString();
        }

        public static string Get(IEnumerable<string> list)
        {
            return "[" + string.Join(", ", list) + "]";
        }
    }
}