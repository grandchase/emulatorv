﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KCCMonsterInfo
    {
        public enum EVariable
        {
            EV_CHAMPIONRATIO = 0,
            EV_MONSTERABILITY = 1,

            EV_MAX,
        };

        public SortedDictionary<int, float> ChampionRatio { get; set; }
        public SortedDictionary<int, KMonsterAbility> MonsterAbility { get; set; }
    }
}
