﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KCCGameCategoryInfo
    {
        public enum EVariable
        {
            EV_CHANNELCATEGORIES = 0,
            EV_CATEGORYMODES = 1,
            EV_CATEGORYINFO = 2,

            EV_MAX,
        };

        public List<Tuple<int, List<int>>> ChannelCategories { get; set; }
        public List<Tuple<int, List<int>>> CategoryModes { get; set; }
        public List<Tuple<Tuple<int, bool>, List<int>>> CategoryInfo { get; set; }
    }
}
