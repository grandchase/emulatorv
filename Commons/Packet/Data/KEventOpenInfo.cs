﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KEventOpenInfo
    {
        public uint EventID { get; set; }
        public uint MBoxID { get; set; }
        public string FileName { get; set; }
        public List<uint> ItemList { get; set; }
    }
}
