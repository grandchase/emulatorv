﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KCCGameCharInfo
    {
        public enum EVariable
        {
            EV_MAXLEVEL = 0,
            EV_CHARACTES = 1,
            EV_CHARSP4OPENINFO = 2,
            EV_CHAROPENTYPE = 3,
            EV_CHARCASHGOODS = 4,
            EV_CHARSKILLENABLE = 5,

            EV_MAX,
        };

        public int MaxLevel { get; set; }
        public SortedDictionary<int, SortedSet<char>> Charactes { get; set; }
        public List<int> CharSP4OpenInfo { get; set; }
        public SortedDictionary<int, uint> CharOpenType { get; set; }
        public SortedDictionary<int, uint> CharCashGoods { get; set; }
        public SortedSet<uint> CharSkillEnable { get; set; }

    }
}
