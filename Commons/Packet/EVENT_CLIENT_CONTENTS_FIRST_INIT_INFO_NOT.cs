﻿using Commons.Packets.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets
{
    public struct EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT
    {
        public enum ECCFIISendType
        {
            ECCFII_CONNECTED = 0,    // 게임-센터 연결시 보내는 패킷
            ECCFII_DATA_CHANGED = 1,    // 변경사항 발생시 보내는 패킷
            ECCFII_MAX,
        };

        public int SendType { get; set; }

        public IList<KCCUsingFilesInfo> UsingFilesInfo { get; set; }
        public IList<KCCHackCheckInfo> HackCheckInfo { get; set; }
    }
}
