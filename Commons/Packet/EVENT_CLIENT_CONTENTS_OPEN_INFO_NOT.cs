﻿using Commons.Packets.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets
{
    public struct EVENT_CLIENT_CONTENTS_OPEN_INFO_NOT
    {
        public enum ECCOISendType
        {
            ECCOI_CONNECTED = 0,    // 게임-센터 연결시 보내는 패킷
            ECCOI_DATA_CHANGED = 1,    // 변경사항 발생시 보내는 패킷

            ECCOI_MAX,
        };

        public int SendType { get; set; }
        public Tuple<List<int>, KCCGameCategoryInfo> GameCategoryInfo { get; set; }
        public Tuple<List<int>, KCCGameCharInfo> GameCharInfo { get; set; }
        public Tuple<List<int>, KCCMonsterInfo> MonsterInfo { get; set; }
        public Tuple<List<int>, KCCMiniEventnInfo> MiniEventInfo { get; set; }
    }
}
