﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KMonsterAbility
    {
        public int MonsterNum { get; set; }
        public int AttackRatio { get; set; }
        public int DefenceRatio { get; set; }
        public int HPRatio { get; set; }
    }
}
