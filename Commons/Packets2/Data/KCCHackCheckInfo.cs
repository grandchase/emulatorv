﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KCCHackCheckInfo
    {
        public enum EVariable
        {
            EV_EXCEPTIONMOTIONID = 0,
            EV_DLLBLACKLIST = 1,
            EV_EXTENDSHALIST = 2,

            EV_MAX,
        };

        public List<int> ExceptionMotionID { get; set; }
        public SortedSet<string> DLLBlackList { get; set; }
        public List<string> ExtendSHAList { get; set; }
    }
}
