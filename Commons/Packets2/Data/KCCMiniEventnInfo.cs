﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KCCMiniEventnInfo
    {
        public enum EVariable
        {
            EV_DISABLEMENULIST = 0,
            EV_EVENTOPENINFO = 1,

            EV_MAX,
        };

        public List<int> DisableMenuList { get; set; }
        public List<KEventOpenInfo> EventOpenInfo { get; set; }
    }
}
