﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Packets.Data
{
    public struct KCCUsingFilesInfo
    {
        public enum ScriptType
        {
            SCF_SQUARE = 0,
            SCF_SQUAREOBJECT = 1,
            SCF_SQUARE3DOBJECT = 2,
        };

        public enum EVariable
        {
            EV_LOADINGIMAGENAME = 0,
            EV_NEWSNUMBER = 1,
            EV_PVPLOADINGIMAGENAME = 2,
            EV_EVENTBANNERINFO = 3,
            EV_SCRIPTNAME = 4,

            EV_MAX,
        };

        public SortedDictionary<int, List<string>> LoadingImageName { get; set; }
        public List<int> NewsNumber { get; set; }
        public SortedDictionary<int, List<string>> PVPLoadingImageName { get; set; }
        public List<Tuple<string, int>> EventBannerInfo { get; set; }
        public SortedDictionary<int, string> ScriptName { get; set; }
    }
}
