﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Commons.Logging;
using Commons.Utilities;
using MoonSharp.Interpreter;

namespace Center.LUA
{
    public static class ClientContents
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Lua, "ClientContents");
        public static int MaxLevel { get; private set; }
        public static List<int> ChannelNewsImageNumber { get; private set; } = new List<int>();
        public static List<string> HashCheckList { get; private set; } = new List<string>();
        public static List<List<string>> LoadingImageFileName { get; private set; } = new List<List<string>>();
        public static List<List<string>> PVPLoadingImageFileName { get; private set; } = new List<List<string>>();
        public static List<List<string>> ClientScriptName { get; private set; } = new List<List<string>>();
        public static Dictionary<string, int> EventBannerTexture { get; private set; } = new Dictionary<string, int>();
        public static List<string> DLLBlackList { get; private set; } = new List<string>();
        public static List<string> ExtendSHAFileList { get; private set; } = new List<string>();
        
        public static void Load(Script script)
        {
            MaxLevel = (int) script.Globals.Get("MaxLevel").Number;
            Logger.Debug($"MexLevel: {MaxLevel}");
            
            foreach (var imageNumber in script.Globals.Get("ChannelNewsImageNumber").Table.Values)
            {
                ChannelNewsImageNumber.Add((int) imageNumber.Number);
            }
            Logger.Debug($"ChannelNewsImageNumber: [{string.Join(", ", ChannelNewsImageNumber)}]");
            
            foreach (var file in script.Globals.Get("HashCheckList").Table.Values)
            {
                HashCheckList.Add(file.String);
            }
            Logger.Debug($"HashCheckList: [{string.Join(", ", HashCheckList)}]");
            
            foreach (var list in script.Globals.Get("LoadingImageFileName").Table.Values)
            {
                var newList = list.Table.Values.Select(file => file.String).ToList();
                LoadingImageFileName.Add(newList);
            }
            Logger.Debug($"LoadingImageFileName: {ListToString.Get(LoadingImageFileName)}");
            
            foreach (var list in script.Globals.Get("PVPLoadingImageFileName").Table.Values)
            {
                var newList = list.Table.Values.Select(file => file.String).ToList();
                PVPLoadingImageFileName.Add(newList);
            }
            Logger.Debug($"PVPLoadingImageFileName: {ListToString.Get(PVPLoadingImageFileName)}");
            
            foreach (var list in script.Globals.Get("ClientScriptName").Table.Values)
            {
                var newList = list.Table.Values.Select(file => file.String).ToList();
                ClientScriptName.Add(newList);
            }
            Logger.Debug($"ClientScriptName: {ListToString.Get(ClientScriptName)}");
            
            foreach (var list in script.Globals.Get("EVENT_BANNER_TEXTURE").Table.Values)
            {
                EventBannerTexture.Add(list.Table.Values.First().String, (int)list.Table.Values.Last().Number);
            }
            Logger.Debug($"EventBannerTexture: {ListToString.Get(EventBannerTexture)}");
            
            foreach (var item in script.Globals.Get("DLLBlackList").Table.Values)
            {
                DLLBlackList.Add(item.String);
            }
            Logger.Debug($"DLLBlackList: {ListToString.Get(DLLBlackList)}");
            
            foreach (var item in script.Globals.Get("ExtendSHAFileList").Table.Values)
            {
                ExtendSHAFileList.Add(item.String);
            }
            Logger.Debug($"ExtendSHAFileList: {ListToString.Get(ExtendSHAFileList)}");
        }
    }
}