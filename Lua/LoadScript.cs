﻿using System;
using System.Collections.Generic;
using System.IO;
using Commons.Logging;
using MoonSharp.Interpreter;

namespace Commons.LUA
{
    public static class LoadScript
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Lua, "LoadScript");
        public static Script Load(string filePath)
        {
            Logger.Lua($"Loading: {filePath}");
            var script = new Script();
            UserData.RegisterType<List<string>>();
            script.DoFile(Path.Combine(Environment.CurrentDirectory, filePath));
            return script;
        }
    }
}