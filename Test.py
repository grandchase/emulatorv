import socket

from struct import *

HOST = '127.0.0.1'    # The remote host
PORT = 1010              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

packet = pack('H', 10)

print repr(packet)
s.send(packet)

while True:
    print "ok"
    data = s.recv(1024)
    
    print 'Received', repr(data)

    if data == "Hey Device!":
        s.send(b'I\'m here')

    
