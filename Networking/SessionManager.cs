﻿using System.Collections.Concurrent;
using DotNetty.Transport.Channels;
using Networking.Sessions;

namespace Networking
{
    public class SessionManager
    {
        private readonly ConcurrentDictionary<IChannelId, Session> _sessions = new ConcurrentDictionary<IChannelId, Session>();

        public bool Add(IChannelHandlerContext context, Session session)
        {
            return !_sessions.ContainsKey(context.Channel.Id) && _sessions.TryAdd(context.Channel.Id, session);
        }

        public bool Remove(IChannelHandlerContext context)
        {
            return _sessions.ContainsKey(context.Channel.Id) && _sessions.TryRemove(context.Channel.Id, out var session);
        }
    }
}