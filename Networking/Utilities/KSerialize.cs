﻿using Networking.Packet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Utilities
{
    public static class KSerialize
    {
        public static void List(this PacketSend packetSend, List<List<string>> list)
        {
            packetSend.Int(list.Count);  // map size 하나밖에 없다. 두개보내면 랜덤일지도
            for (var i = 0; i < list.Count; i++)
            {
                packetSend.Int(i); // map index
                packetSend.Int(list[i].Count); // vector size
                foreach (var imageName in list[i])
                {
                    packetSend.UnicodeStr(imageName);
                }
            }
        }
    }
}
