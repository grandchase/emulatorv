﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Commons.Utilities;
using Networking.Utilities;

namespace Networking.Packet
{
    public class PacketWrite : IDisposable
    {
        private readonly List<byte> _byteList;

        private int Position { get; set; }
        
        public byte[] Packet => _byteList.ToArray();

        public PacketWrite()
        {
            _byteList = new List<byte>();
            Position = 0;
        }

        private void Inc(IReadOnlyCollection<byte> data)
        {
            _byteList.AddRange(data);
            Position += data.Count;
        }

        public void Byte(byte _byte)
        {
            Inc(new[] {_byte});
        }

        public void Bool(bool boolean)
        {
            Inc(new[] {Convert.ToByte(boolean)});
        }

        public void Short(short int16)
        {
            Inc(BigEndian.GetBytes(int16));
        }

        public void UShort(ushort uint16)
        {
            Inc(BigEndian.GetUInt16(uint16));
        }

        public void Int(int int32)
        {
            Inc(BigEndian.GetBytes(int32));
        }

        public void UInt(uint uint32)
        {
            Inc(BigEndian.GetBytes(uint32));
        }

        public void Long(long Long)
        {
            Inc(BigEndian.GetBytes(Long));
        }

        public void UnicodeStr(string unicodeString)
        {
            Int(unicodeString.Length * 2);
            Inc(Encoding.Unicode.GetBytes(unicodeString));
        }

        public void Str(string str)
        {
            Int(str.Length);
            Inc(Encoding.ASCII.GetBytes(str));
        }

        public void WriteIP(string serverIp)
        {
            var reverseServerIp = IPAddress.Parse(serverIp).GetAddressBytes();
            Array.Reverse(reverseServerIp);
            ArrayBytes(reverseServerIp);
        }

        public void ArrayBytes(byte[] bytes)
        {
            Inc(bytes);
        }

        public void HexArray(string bytes)
        {
            bytes = bytes.Replace(" ", "");
            for (var i = 0; i < bytes.Length / 2; i++) Byte(Convert.ToByte(bytes.Substring(i * 2, 2), 16));
        }

        public void HexArray(IEnumerable<byte> bytes)
        {
            foreach (var _byte in bytes) Byte(_byte);
        }

        public void Dispose()
        {
            _byteList.Clear();
        }
    }
}