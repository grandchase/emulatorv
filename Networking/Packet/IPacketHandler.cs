﻿using Networking.Sessions;

namespace Networking.Packet
{
    public interface IPacketHandler
    {
        void Dispatch(Session client, PacketRead packet);
    }
}