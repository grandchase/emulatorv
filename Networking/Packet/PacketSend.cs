﻿using System;
using System.Xml;

namespace Networking.Packet
{
    public abstract class PacketSend : IDisposable
    {
        protected PacketSend(short code)
        {
            Code = code;
        }
        public short Code { get; }
        private readonly PacketWrite _write = new PacketWrite();
        public byte[] Packet => _write.Packet;
        public void ArrayBytes(byte[] bytes) => _write.ArrayBytes(bytes);
        public void Int(int code) => _write.Int(code);
        public void Short(short code) => _write.Short(code);
        public void UnicodeStr(string str) => _write.UnicodeStr(str);
        public void Dispose() => _write.Dispose();
    }
}