﻿using System;
using System.Text;
using Commons.Utilities;
using Ionic.Zlib;
using Networking.Utilities;

namespace Networking.Packet
{
    public class PacketRead
    {
        private int Position { get; set; }

        public PacketRead(byte[] buffer, int position = 7, bool check = false)
        {
            try
            {
                Position = position;
                            Buffer = buffer;
                            if (check != true) return;
                            if (buffer.Length <= 7) return;
                            if (buffer[6] == 1)
                            {
                                Position = 0;
                                Jump(11);
                                var zlibSize = (ushort) ((Buffer[4] << 8) | Buffer[5]);
                                var contentCompress = Buffer_Array_Bytes(zlibSize - 4);
                                Position = 0;
                                Buffer = null;
                                Buffer = ZlibStream.UncompressBuffer(contentCompress);
                            }
                            else
                            {
                                Position = 7;
                                Buffer = buffer;
                            }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        private byte[] Buffer { get; }

        public byte[] Buffer_Array_Bytes(int size)
        {
            var currentBuffer = new byte[size];
            Array.Copy(Buffer, Position, currentBuffer, 0, size);
            Position += size;
            return currentBuffer;
        }

        public void Jump(int size)
        {
            Position += size;
        }

        public short Code()
        {
            return (short) ((Buffer[0] << 8) | Buffer[1]);
        }

        public byte Byte()
        {
            Position += 1;
            return Buffer[Position - 1];
        }

        public bool Bool()
        {
            Position += 1;
            return Buffer[Position - 1] == 1;
        }

        public int Int()
        {
            return BigEndian.GetInt32(Buffer_Array_Bytes(sizeof(int)), 0);
        }

        public uint UInt()
        {
            return BigEndian.GetUInt32(Buffer_Array_Bytes(sizeof(uint)), 0);
        }

        public short Short()
        {
            return BigEndian.GetInt16(Buffer_Array_Bytes(sizeof(short)), 0);
        }

        public ushort UShort()
        {
            return (ushort) BigEndian.GetInt16(Buffer_Array_Bytes(sizeof(ushort)), 0);
        }

        public long Long()
        {
            return BigEndian.GetInt64(Buffer_Array_Bytes(sizeof(long)), 0);
        }

        public string UnicodeString()
        {
            var sizeUString = Int();
            return Encoding.Unicode.GetString(Buffer_Array_Bytes(sizeUString));
        }

        public string String()
        {
            var sizeString = Int();
            return Encoding.ASCII.GetString(Buffer_Array_Bytes(sizeString));
        }
    }
}