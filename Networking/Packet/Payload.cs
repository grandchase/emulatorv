﻿namespace Networking.Packet
{
    public class Payload
    {
        public Payload(byte[] content, short packetId, bool isCompressed)
        {
            var writer = new PacketWrite();
            writer.Short(packetId);
            writer.Int(content.Length);
            writer.Bool(isCompressed);
            writer.ArrayBytes(content);
            Data = writer.Packet;
        }

        public Payload(byte[] payload)
        {
            Data = payload;
        }

        public byte[] Data { get; }
    }
}