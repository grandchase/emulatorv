﻿using System;
using System.Collections.Generic;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using Networking.Crypto;
using Networking.Packet;

namespace Networking.Codec
{
    public class PacketDecoder : MessageToMessageDecoder<IByteBuffer>
    {
        private Cryptography Cryptography { get; }
        public PacketDecoder(Cryptography cryptography)
        {
            Cryptography = cryptography;
        }
        protected override void Decode(IChannelHandlerContext context, IByteBuffer message, List<object> output)
        {
            try
            {
                var position = 0;
                var dataReceived = new byte[message.ReadableBytes];
                message.ReadBytes(dataReceived);
                var packetRead = new PacketRead(dataReceived, 0);
                var shortSize = packetRead.UShort();
                while (position < dataReceived.Length)
                {
                    var buffer = new byte[shortSize];
                    Array.Copy(dataReceived, position, buffer, 0, shortSize);
                    var newPacketRead = new PacketRead(buffer, 0);
                    position += (ushort) ((dataReceived[position + 1] << 8) | dataReceived[position]);
                    var newSize = newPacketRead.UShort();
                    newPacketRead.Short();
                    newPacketRead.Int();
                    var iv = newPacketRead.Buffer_Array_Bytes(8);
                    var content = newPacketRead.Buffer_Array_Bytes(newSize - 16 - 10);
                    var getPayload = Cryptography.Decrypt(content, iv);
                    var payloadPacketRead = new PacketRead(getPayload, 0, true);
                    var packetId = (ushort) ((getPayload[0] << 8) | getPayload[1]);
                    output.Add(new NetworkInMessage {Code = packetId, PacketRead = payloadPacketRead});
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}