﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Commons.Utilities;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using Networking.Crypto;
using Networking.Packet;
using Networking.Utilities;

namespace Networking.Codec
{
    public class PacketEncoder : MessageToByteEncoder<NetworkOutMessage>
    {
        private Cryptography Cryptography { get; }
        public PacketEncoder(Cryptography cryptography)
        {
            Cryptography = cryptography;
        }
        protected override void Encode(IChannelHandlerContext context, NetworkOutMessage outMessage, IByteBuffer output)
        {
            var payload = new Payload(outMessage.PacketSend.Packet, outMessage.PacketSend.Code, outMessage.Flag);
            const int count = 1;
            var generateIv = new byte[8];
            var random = new Random();
            var byteTemp = (byte) random.Next(0x00, 0xFF);
            for (var i = 0; i < generateIv.Length; i++) generateIv[i] = byteTemp;
            var encryptBuffer = Cryptography.Encrypt(payload.Data, generateIv);
            var newBufferSize = (ushort) (16 + encryptBuffer.Length + 10);
            var newHmac = new HMACMD5(Cryptography.AuthenticKey);
            var concatBuffer = Sequence.Concat(BitConverter.GetBytes(outMessage.Prefix), BitConverter.GetBytes(count),
                generateIv, encryptBuffer);
            var authCode =
                Sequence.ReadBlock(
                    newHmac.ComputeHash(Sequence.Concat(BitConverter.GetBytes(outMessage.Prefix), BitConverter.GetBytes(count),
                        generateIv, encryptBuffer)), 0, 10);
            var bufferResult = Sequence.Concat(BitConverter.GetBytes(newBufferSize), concatBuffer, authCode);
            output.WriteBytes(bufferResult);
        }
    }
}