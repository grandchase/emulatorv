﻿using System;
using Commons.Logging;
using DotNetty.Transport.Channels;
using Networking.Codec;
using Networking.Crypto;
using Networking.Packet;
using Networking.Sessions;

namespace Networking
{
    public class NetworkHandler : ChannelHandlerAdapter
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Networking, "Handler");
        private IServerHandler ServerHandler { get; }
        private SessionManager SessionManager { get; }
        private Session Session { get; }
        public NetworkHandler(Session session, IServerHandler serverHandler, SessionManager sessionManager)
        {
            Session = session;
            ServerHandler = serverHandler;
            SessionManager = sessionManager;
        }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            if (SessionManager.Add(context, Session))
            {
                ServerHandler.ChannelActive(Session);
            }
        }

        public override void ChannelInactive(IChannelHandlerContext context)
        {
            ServerHandler.ChannelInactive(Session);
            if (!SessionManager.Remove(context))
            {
                Logger.Warn($"Can't remove a session: {Session.Address}");
            }
        }

        public override void ChannelRead(IChannelHandlerContext ctx, object output)
        {
            if (!(output is NetworkInMessage message)) return;
            ServerHandler.ChannelRead(Session, message.Code, message.PacketRead);
        }
        
        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Logger.Error($"{exception.Message}{exception.StackTrace}");
        }
    }
}