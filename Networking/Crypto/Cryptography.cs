﻿using System.Security.Cryptography;
using Commons.Utilities;
using Networking.Utilities;

namespace Networking.Crypto
{
    public class Cryptography
    {
        private readonly DESCryptoServiceProvider _des = new DESCryptoServiceProvider
        {
            Mode = CipherMode.CBC,
            Padding = PaddingMode.None
        };

        public byte[] CryptographyKey { get; set; } = {0xC7, 0xD8, 0xC4, 0xBF, 0xB5, 0xE9, 0xC0, 0xFD};

        public byte[] AuthenticKey { get; set; } = {0xC0, 0xD3, 0xBD, 0xC3, 0xB7, 0xCE, 0xB8, 0xB8};

        public byte[] RNGCryptoServiceProvider()
        {
            var generateKey = new byte[8];
            var rangeCryptoGenerate = new RNGCryptoServiceProvider();
            rangeCryptoGenerate.GetBytes(generateKey);
            return generateKey;
        }

        public byte[] Decrypt(byte[] buffer, byte[] IV)
        {
            using (var decryptor = _des.CreateDecryptor(CryptographyKey, IV))
            {
                return decryptor.TransformFinalBlock(buffer, 0, buffer.Length);
            }
        }

        public byte[] Encrypt(byte[] data, byte[] IV)
        {
            var dataToEncrypt = PadData(data);
            using (var encryptor = _des.CreateEncryptor(CryptographyKey, IV))
            {
                return encryptor.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length);
            }
        }

        private byte[] PadData(byte[] data)
        {
            var distance = 8 - data.Length % 8;
            var paddingLength = distance >= 3 ? distance : 8 + distance;
            var padding = new byte[paddingLength];
            for (var i = 0; i < paddingLength - 1; i++) padding[i] = (byte) (i + 1);
            padding[paddingLength - 1] = padding[paddingLength - 2];
            return Sequence.Concat(data, padding);
        }
    }
}