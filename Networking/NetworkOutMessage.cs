﻿using Networking.Packet;

namespace Networking
{
    public class NetworkOutMessage
    {
        public PacketSend PacketSend { get; set; }
        public bool Flag { get; set; }
        public short Prefix { get; set; }
    }
}