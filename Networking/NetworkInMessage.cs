﻿using Networking.Packet;

namespace Networking
{
    public class NetworkInMessage
    {
        public ushort Code { get; set; }
        public PacketRead PacketRead { get; set; }
    }
}