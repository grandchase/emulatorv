﻿using Networking.Packet;
using Networking.Sessions;

namespace Networking
{
    public interface IServerHandler
    {
        void ChannelActive(Session session);
        void ChannelInactive(Session session);
        void ChannelRead(Session session, ushort packetId, PacketRead packetRead);
    }
}