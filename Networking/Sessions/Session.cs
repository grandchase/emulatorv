﻿using System;
using System.Net;
using DotNetty.Transport.Channels;
using Networking.Crypto;
using Networking.Packet;

namespace Networking.Sessions
{
    public class Session
    {
        private IChannel Channel { get; }
        
        public readonly Cryptography Cryptography = new Cryptography();
        public string Address => ((IPEndPoint) Channel.RemoteAddress).Address.MapToIPv4().ToString();
        public Session(IChannel channel)
        {
            Channel = channel;
        }
        public void Send(PacketSend packetSend, bool flag = false, short prefix = -1)
        {
            if (!Channel.IsWritable) return;
            Channel.WriteAndFlushAsync(new NetworkOutMessage
            {
                PacketSend = packetSend,
                Flag = flag,
                Prefix = prefix
            });
        }
    }
}