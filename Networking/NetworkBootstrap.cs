﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading.Tasks;
using Commons.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Networking.Codec;
using Networking.Sessions;
using Settings.Center;
using Settings.Networking;

namespace Networking
{
    public class NetworkBootstrap
    {
        private readonly Logger _logger = LogManager.Instance(LoggerLocation.Networking, "Bootstrap");
        private readonly SessionManager _sessionManager = new SessionManager();
        public async Task Bind(IServerHandler serverHandler, ServerNetwork settings)
        {
            try
            {
                var bossGroup = new MultithreadEventLoopGroup(settings.Boss);
                _logger.Debug($"Boss group event loop count: {settings.Boss}");
                var workerGroup = new MultithreadEventLoopGroup(settings.Worker);
                _logger.Debug($"Worker group event loop count: {settings.Worker}");
                var bootstrap = new ServerBootstrap()
                    .Group(bossGroup, workerGroup)
                    .Channel<TcpServerSocketChannel>()
                    .ChildHandler(new ActionChannelInitializer<ISocketChannel>(channel =>
                    {
                        var session = new Session(channel);
                        channel.Pipeline.AddLast(new PacketEncoder(session.Cryptography));
                        channel.Pipeline.AddLast(new PacketDecoder(session.Cryptography));
                        channel.Pipeline.AddLast(new NetworkHandler(session, serverHandler, _sessionManager));
                    }))
                    .ChildOption(ChannelOption.AutoRead, true)
                    .ChildOption(ChannelOption.SoKeepalive, true)
                    .ChildOption(ChannelOption.TcpNodelay, true)
                    .ChildOption(ChannelOption.SoBacklog, settings.Backlog)
                    .ChildOption(ChannelOption.SoRcvbuf, settings.BufferSize)
                    .ChildOption(ChannelOption.SoSndbuf, settings.BufferSize);
                await bootstrap.BindAsync(IPAddress.Any, settings.Port);
                _logger.Info($"Server working on port {settings.Port}");
            }
            catch (Exception exception)
            {
                _logger.Error($"Can't start network: {exception.Message}");
            }
        }
    }
}