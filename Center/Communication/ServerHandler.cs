﻿using System;
using Center.Communication.Messages.Output;
using Center.Communication.Packet;
using Commons.Logging;
using Networking;
using Networking.Packet;
using Networking.Sessions;

namespace Center.Communication
{
    public class ServerHandler : IServerHandler
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Center, "ServerHandler");
        public void ChannelActive(Session session)
        {
            Logger.Debug($"New connection: {session.Address}");
            var cryptography = session.Cryptography;
            var cryptoAuth = cryptography.RNGCryptoServiceProvider();
            var cryptoKey = cryptography.RNGCryptoServiceProvider();
            session.Send(new SET_SECURITY_KEY_NOT(cryptoAuth, cryptoKey));
            cryptography.AuthenticKey = cryptoAuth;
            cryptography.CryptographyKey = cryptoKey;
            session.Send(new ENU_WAIT_TIME_NOT(100));
        }

        public void ChannelInactive(Session session)
        {
            Logger.Debug($"Lost connection: {session.Address}");
        }

        public void ChannelRead(Session session, ushort packetId, PacketRead packetRead)
        {
            Console.WriteLine($"[{packetId}]: {((Opcodes) packetId)}");
            if (PacketProcessor.TryGetPacket(packetId, out var packetHandler))
            {
                packetHandler.Dispatch(session, packetRead);
            }
            else
            {
                Logger.Warn($"Packet not found [{packetId}]: {((Opcodes) packetId)}");
            }
        }
    }
}