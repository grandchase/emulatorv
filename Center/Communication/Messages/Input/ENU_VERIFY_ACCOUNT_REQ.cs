﻿using System;
using Commons.Attributes;
using Networking.Packet;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn((ushort) Opcodes.ENU_VERIFY_ACCOUNT_REQ)]
    public class ENU_VERIFY_ACCOUNT_REQ : IPacketHandler
    {
        public void Dispatch(Session client, PacketRead packet)
        {
            packet.Int();
            var strLogin = packet.String();
            var strPassword = packet.String();
        }
    }
}