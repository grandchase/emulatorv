﻿using System;
using System.Collections.Generic;
using Center.Communication.Messages.Output;
using Center.LUA;
using Commons.Attributes;
using Commons.Packets;
using Commons.Packets.Data;
using Networking.Packet;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn((ushort) Opcodes.ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_REQ)]
    public class ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_REQ : IPacketHandler
    {
        public void Dispatch(Session client, PacketRead packet)
        {
            var EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT = new EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT();
            EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.SendType = (int) EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.ECCFIISendType.ECCFII_CONNECTED;

            EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.UsingFilesInfo = new List<KCCUsingFilesInfo>();

            var KCCUsingFilesInfo = new KCCUsingFilesInfo
            {
                LoadingImageName = new SortedDictionary<int, List<string>>(),
                NewsNumber = new List<int>(),
                PVPLoadingImageName = new SortedDictionary<int, List<string>>(),
                EventBannerInfo = new List<Dictionary<string, int>>(),
                ScriptName = new SortedDictionary<int, string>()
            };

            var LoadingImageFileName = ClientContents.LoadingImageFileName;
            for (int i = 0; i < LoadingImageFileName.Count; i++)
            {
                KCCUsingFilesInfo.LoadingImageName.Add(i, LoadingImageFileName[i]);
            }

            var NewsNumber = ClientContents.ChannelNewsImageNumber;
            foreach (var Number in NewsNumber)
            {
                KCCUsingFilesInfo.NewsNumber.Add(Number);
            }

            var PVPLoadingImageName = ClientContents.PVPLoadingImageFileName;
            for (int i = 0; i < PVPLoadingImageName.Count; i++)
            {
                KCCUsingFilesInfo.PVPLoadingImageName.Add(i, PVPLoadingImageName[i]);
            }

            var EventBannerInfo = ClientContents.EventBannerTexture;
            for (int i = 0; i < EventBannerInfo.Count; i++)
            {
                KCCUsingFilesInfo.EventBannerInfo.Add(EventBannerInfo);
            }

            var ScriptName = ClientContents.EventBannerTexture;
            foreach (var Script in ScriptName)
            {
                KCCUsingFilesInfo.ScriptName.Add(Script.Value, Script.Key);
            }

            EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.UsingFilesInfo.Add(KCCUsingFilesInfo);

            var KCCHackCheckInfo = new KCCHackCheckInfo
            {
                ExceptionMotionID = new List<int>(),
                DLLBlackList = new SortedSet<string>(),
                ExtendSHAList = new List<string>()
            };

            var DLLBlackList = ClientContents.DLLBlackList;
            foreach (var DLLBlack in DLLBlackList)
            {
                KCCHackCheckInfo.DLLBlackList.Add(DLLBlack);
            }

            var ExtendSHAList = ClientContents.ExtendSHAFileList;
            foreach (var ExtendSHA in ExtendSHAList)
            {
                KCCHackCheckInfo.ExtendSHAList.Add(ExtendSHA);
            }

            EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.HackCheckInfo.Add(KCCHackCheckInfo);

            client.Send(new ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_ACK(EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT));
        }
    }
}