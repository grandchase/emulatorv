﻿using Commons.Attributes;
using Networking.Packet;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn((ushort) Opcodes.HEART_BIT_NOT)]
    public class HEART_BIT_NOT : IPacketHandler
    {
        public void Dispatch(Session client, PacketRead packet)
        {
        }
    }
}