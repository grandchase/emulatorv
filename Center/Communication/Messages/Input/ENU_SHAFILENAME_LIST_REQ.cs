﻿using Center.Communication.Messages.Output;
using Commons.Attributes;
using Networking.Packet;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn((ushort) Opcodes.ENU_SHAFILENAME_LIST_REQ)]
    public class ENU_SHAFILENAME_LIST_REQ : IPacketHandler
    {
        public void Dispatch(Session client, PacketRead packet)
        {
            //client.Send(new ENU_SHAFILENAME_LIST_ACK(Startup.ClientContents.HashCheckList));
        }
    }
}