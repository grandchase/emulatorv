﻿using Networking.Packet;

namespace Center.Communication.Messages.Output
{
    public class ENU_WAIT_TIME_NOT : PacketSend
    {
        public ENU_WAIT_TIME_NOT(int time) : base((short) Opcodes.ENU_WAIT_TIME_NOT)
        {
            Int(time);
        }
    }
}