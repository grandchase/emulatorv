﻿using System.Collections.Generic;
using Networking.Packet;

namespace Center.Communication.Messages.Output
{
    public class ENU_SHAFILENAME_LIST_ACK : PacketSend
    {
        public ENU_SHAFILENAME_LIST_ACK(IReadOnlyCollection<string> list) : base((short) Opcodes.ENU_SHAFILENAME_LIST_ACK)
        {
            Int(0);
            Int(list.Count);
            foreach (var file in list)
            {
                UnicodeStr(file);
            }
        }
    }
}