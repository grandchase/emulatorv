﻿using System;
using System.Collections.Generic;
using Commons.Packets;
using Networking.Packet;

namespace Center.Communication.Messages.Output
{
    public class ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_ACK : PacketSend
    {
	    public ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_ACK(EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT) : base(
		    (short) Opcodes.ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_ACK)
	    {

			Int(EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.SendType); // 0 = ECCFII_CONNECTED / 1 = ECCFII_DATA_CHANGED

			var usingfiles = EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.UsingFilesInfo;
			Int(usingfiles.Count); // 아마 맞을듯;; 5

			for (int fileInfoIndex = 0; fileInfoIndex < usingfiles.Count; fileInfoIndex++)
			{
				Int(fileInfoIndex);
			}
			for (int fileInfoIndex = 0; fileInfoIndex < usingfiles.Count; fileInfoIndex++)
			{
				Int(fileInfoIndex);
				var usingFile = usingfiles[fileInfoIndex];

				var loadingImages = usingFile.LoadingImageName;
				// m_mapLoadingImageName
				Int(loadingImages.Count);
				for (var landingImageIndex = 0; landingImageIndex < loadingImages.Count; landingImageIndex++)
                {
					Int(landingImageIndex);
					Int(loadingImages[landingImageIndex].Count);
					foreach (var imageName in loadingImages[landingImageIndex])
                    {
						UnicodeStr(imageName);
					}
				}

				var channelNewsImageNumber = usingFile.NewsNumber;
				// m_vecNewsNumber
				Int(channelNewsImageNumber.Count); // vector size
				foreach (var imageNumber in channelNewsImageNumber)
				{
					Int(imageNumber); // news
				}

				var pvpLoadingImageName = usingFile.PVPLoadingImageName;
				// m_mapPVPLoadingImageName
				Int(pvpLoadingImageName.Count); // map size
				for (var i = 0; i < pvpLoadingImageName.Count; i++)
				{
					Int(i); // map index
					Int(pvpLoadingImageName[i].Count); // vector size
					foreach (var imageName in pvpLoadingImageName[i])
					{
						UnicodeStr(imageName);
					}
				}

				var eventBannerTexture = usingFile.EventBannerInfo;
				// m_vecEventBannerInfo
				Int(eventBannerTexture.Count); // vector size
				foreach (var eventBanner in eventBannerTexture)
				{
					foreach (var texture in eventBanner)
					{
						UnicodeStr(texture.Key);
						Int(texture.Value);
					}
				}

				var clientScriptName = usingFile.ScriptName;
				// map : m_mapScriptName
				Int(clientScriptName.Count); // map size 또 하나다
				for (var i = 0; i < clientScriptName.Count; i++)
				{
					Int(i); // map index
					Int(clientScriptName[i].Length); // vector size
					foreach (var imageName in clientScriptName[i])
					{
						UnicodeStr(imageName.ToString());
					}
				}
			}


			// exception Model
			var hackFiles = EVENT_CLIENT_CONTENTS_FIRST_INIT_INFO_NOT.HackCheckInfo;
			Int(hackFiles.Count); // 아마 맞을듯;; 5

			for (int hackFilesIndex = 0; hackFilesIndex < hackFiles.Count; hackFilesIndex++)
			{
				Int(hackFilesIndex);
			}
			for (int hackFilesIndex = 0; hackFilesIndex < hackFiles.Count; hackFilesIndex++)
			{
				var hackFile = hackFiles[hackFilesIndex];

				// vector<integer> : m_vecExceptionMotionID
				Int(hackFile.ExceptionMotionID.Count);
				foreach (var motionID in hackFile.ExceptionMotionID)
                {
					Int(motionID);
                }

				// set<wide string> : m_setDLLBlackList
				Int(hackFile.DLLBlackList.Count); // size
				foreach (var file in hackFile.DLLBlackList)
				{
					UnicodeStr(file);
				}

				// vector<wide string> : m_vecExtendSHAList
				Int(hackFile.ExceptionMotionID.Count); // 테스트
				foreach (var file in hackFile.ExtendSHAList)
				{
					UnicodeStr(file);
				}
			}
		}
    }
}