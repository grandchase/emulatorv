﻿using Networking.Packet;

namespace Center.Communication.Messages.Output
{
    public class SET_SECURITY_KEY_NOT : PacketSend
    {
        public SET_SECURITY_KEY_NOT(byte[] newCryptoAuth, byte[] newCryptoKey) : base((short) Opcodes.SET_SECURITY_KEY_NOT)
        {
            Short(24787);
            Int(8);
            ArrayBytes(newCryptoAuth);
            Int(8);
            ArrayBytes(newCryptoKey);
            Int(1);
            Int(0);
            Int(0);
        }
    }
}