﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Center.Communication;
using Center.Communication.Packet;
using Center.LUA;
using Commons.Logging;
using Commons.LUA;
using Networking;
using Settings;
using Settings.Center;

namespace Center
{
    internal static class Startup
    {
        private static DateTime Boot { get; set; }
        
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Center, "Startup");
        
        public static readonly XmlSerializer<CenterServer> Settings =
            new XmlSerializer<CenterServer>();

        public static void Main(string[] args)
        {
            Thread.Sleep(50);
            Boot = DateTime.Now;
            ClientContents.Load(LoadScript.Load("data/InitClientContents.lua"));
            Logger.Info("Starting server...");
            if (!Settings.Load("data/Settings.xml")) return;
            RunAsync().GetAwaiter().GetResult();
        }

        private static async Task RunAsync()
        {
            var bootstrap = new NetworkBootstrap();
            PacketProcessor.Initialize();
            await bootstrap.Bind(new ServerHandler(), Settings.Data.ServerNetwork);
            var seconds = DateTime.Now - Boot;
            Logger.Info($"Server started in {seconds.TotalSeconds} seconds");
            await Task.Delay(-1);
        }
    }
}