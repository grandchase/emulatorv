MaxLevel = 85

ChannelNewsImageNumber = { 1, 2, 3 }

HashCheckList = { "ai.kom", "main.exe", "script.kom" }

LoadingImageFileName = {
	{ "Load1_1.dds", "Load1_2.dds", "Load1_3.dds", "LoadGauge1.dds" }
}

PVPLoadingImageFileName = {
    {"ui_match_load1.dds",  "ui_match_load2.dds",  "ui_match_load3.dds", }
}

ClientScriptName = {
    { "Square.lua" },
	--{ "Square_festival.lua" },
	--{ "SquareObject_amyfree.lua" },
    --{ "Square3DObject_amyfree.lua" },
    { "SquareObject.lua" },
    { "Square3DObject.lua" },
}

NEW = 0
GOING = 1
END = 2

EVENT_BANNER_TEXTURE = {
	{ "banner1.dds", NEW, },
	{ "banner2.dds", NEW, },
	{ "banner3.dds", GOING, },
	{ "banner4.dds", END, },
--	{ "banner5.dds", GOING, },
--	{ "banner6.dds", GOING, },
--	{ "banner7.dds", GOING, },
--	{ "banner8.dds", GOING, },
}

ExceptionMotionID = { }

DLLBlackList = { "d3d9.dll" }

ExtendSHAFileList = { "Stage/AI/AI.kom" }
