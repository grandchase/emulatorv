﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Settings
{
    public class XmlSerializer<T>
    {
        private readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(T));

        public T Data { get; private set; }

        public bool Load(string path)
        {
            try
            {
                if (!File.Exists(path))
                    using (var file = File.Create(path))
                    {
                        _xmlSerializer.Serialize(file, Activator.CreateInstance(typeof(T)));
                    }

                using (var reader = XmlReader.Create(path))
                {
                    Data = (T) _xmlSerializer.Deserialize(reader);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}