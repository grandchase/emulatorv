﻿using System;
using System.Xml.Serialization;

namespace Settings.Networking
{
    [Serializable]
    public class ServerNetwork
    {
        [XmlElement("Port")] public int Port { get; set; }

        [XmlElement("BufferSize")] public int BufferSize { get; set; }

        [XmlElement("Backlog")] public int Backlog { get; set; }

        [XmlElement("Boss")] public int Boss { get; set; }

        [XmlElement("Worker")] public int Worker { get; set; }
    }
}