﻿using System;
using System.Xml.Serialization;
using Settings.Database;
using Settings.Networking;

namespace Settings.Center
{
    [Serializable]
    public class CenterServer
    {
        [XmlElement("Network")] public ServerNetwork ServerNetwork { get; set; }
        [XmlElement("Database")] public ServerDatabase ServerDatabase { get; set; }

        // Caso o arquivo .xml de configuração não exista, quando for criado será com os valores abaixo.
        public CenterServer()
        {
            ServerNetwork = new ServerNetwork
            {
                Port = 9500,
                BufferSize = 1024,
                Backlog = 100,
                Boss = 4,
                Worker = 2
            };
            ServerDatabase = new ServerDatabase
            {

            };
        }
    }
}